#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Merge Sort
void merge(int *values, int start, int middle, int end) {
    int i = start;
	int j = middle+1;
	int k = 0;
	int dim = end-start+1;
    int *aux;
    aux = (int*) malloc(dim * sizeof(int));

    while(i <= middle && j <= end){
        if(values[i] < values[j]) {
            aux[k] = values[i];
            i++;
        } else {
            aux[k] = values[j];
            j++;
        }
        k++;
    }

    while(i <= middle){
        aux[k] = values[i];
        k++;
        i++;
    }

    while(j <= end) {
        aux[k] = values[j];
        k++;
        j++;
    }

    for(k = start; k <= end; k++){
        values[k] = aux[k-start];
    }
    
    free(aux);
}

void mergeSort(int *values, int start, int end){
    if(start < end){
        int midlle = (start + end) / 2;
        mergeSort(values, start, midlle);
        mergeSort(values, midlle + 1, end);
        merge(values, start, midlle, end);
    }
}

// Quick Sort
void quicksort(int *values, int start, int end){
	int aux;
	int i = start;
	int j = end;
	int sup = values[(start + end + 1) / 2];

	while(i <= j){
		while(values[i] < sup && i < end+1)
		{
			i++;
		}
		while(values[j] > sup && j > start)
		{
			j--;
		}
		if(i <= j)
		{
			aux = values[i];
			values[i] = values[j];
			values[j] = aux;
			i++;
			j--;
		}
	}
	if(j > start)
		quicksort(values, start, j+1);
	if(i < end+1)
		quicksort(values, i, end);
}

// Bubble Sort
void swap(int *a, int *b){ 
    int aux = *a; 
    *a = *b; 
    *b = aux; 
} 

void bubbleSort(int *values, int end){ 
    if (end < 1)return; 
 
    for (int i=0; i < end; i++){
        if (values[i] > values[i+1]){
            swap(&values[i], &values[i+1]);
		}
	}
    bubbleSort(values, end-1); 
} 

// Fluxo principal
int main(int argc, char **argv){
	int size = 10;
	int nElements = 0;
	int *array, *newArray;

	array = (int *) malloc(sizeof(int)*size);
	while(!feof(stdin)){
      if(scanf("%i", (array + nElements)) != 0 && !feof(stdin)){
		nElements++;
	  }

	  if(nElements == size){
		size = size*2;
		newArray = (int *) malloc(sizeof(int)*size);
		for(int i=0; i<nElements; i++) {
			newArray[i] = array[i];
		}
		free(array);
		array = newArray;
	  }
	}

	if(argc == 1) {
		bubbleSort(array, nElements-1);
	}

	for(int i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "-q")) {
			quicksort(array, 0, nElements-1);
			break;
		} 
		else if (!strcmp(argv[i], "-m")) {
			mergeSort(array, 0, nElements-1);
			break;
		} 
		else if (i == argc-1) {
			bubbleSort(array, nElements-1);
			break;
		}
	}
	
	for(int i = 0; i < nElements; i++){
		printf("%d\n", array[i]);
	}
	
    return 0;
}