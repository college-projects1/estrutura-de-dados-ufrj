#include <stdio.h>
#include <stdlib.h>

struct basic_avl_tree
{
    int key;
    struct basic_avl_tree *left_children;
    struct basic_avl_tree *right_children;
    int height;
};

typedef struct basic_avl_tree Tree;

int max(int a, int b) {
    if (a > b) { return a; }
    else { return b; }
}

int getHeight(Tree *T) {
    if (T == NULL) { return -1; }
    return T->height;
}

Tree* right_rotate(Tree *T) {
    Tree *newRoot = T->left_children;
    T->left_children = newRoot->right_children;
    newRoot->right_children = T;
    return newRoot;
}

Tree* left_rotate(Tree *T) {
    Tree *newRoot = T->right_children;
    T->right_children = newRoot->left_children;
    newRoot->left_children = T;
    return newRoot;
}

Tree* left_right_rotate(Tree *T) {
    T->left_children = left_rotate(T->left_children);
    return right_rotate(T);
}

Tree* right_left_rotate(Tree *T) {
    T->right_children = right_rotate(T->right_children);
    return left_rotate(T);
}

Tree* insert(Tree *T, Tree *new) {
    if (T == NULL) {
        return new;
    }

    if (new->key < T->key) {
        T->left_children = insert(T->left_children, new);
        if (getHeight(T->left_children)-getHeight(T->right_children) == 2) {
            if (new->key < T->left_children->key) { T = right_rotate(T); }
            else { T = left_right_rotate(T); }
        }
    } else {
        T->right_children = insert(T->right_children, new);
        if (getHeight(T->right_children)-getHeight(T->left_children) == 2) {
            if (new->key > T->right_children->key) { T = left_rotate(T); }
            else { T = right_left_rotate(T); }
        }
    }

    T->height = max(getHeight(T->left_children), getHeight(T->right_children))+1;

    return T;
}

void print_pre_order(Tree *T) {
    if (T != NULL) {
        printf("%i\n", T->key);
        print_pre_order(T->left_children);
        print_pre_order(T->right_children);
    }
}

int main(int argc, char **argv) {
    Tree *newTree;
    Tree *root = NULL;

    int newValue;
    while(!feof(stdin)){
        if(scanf("%i", &newValue) != 0 && !feof(stdin)){
            newTree = (Tree *) malloc(sizeof(Tree));
            newTree->left_children = NULL;
            newTree->right_children = NULL;
            newTree->key = newValue;
            newTree->height = 0;

            root = insert(root, newTree);
        }
    }
    print_pre_order(root);

    return 0;
}