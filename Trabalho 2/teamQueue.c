#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Node
{
    int value;
    struct Node *next;
};

typedef struct Node tNode;

typedef struct Group
{
    int size;
    int *elements;
} tGroup;

tNode *linkedList;
tGroup *teams;

int linkedListSize = 0;
int teamsSize = 0;

int getTeam(int value)
{
    for (int i = 0; i < teamsSize; i++)
    {
        for (int j = 0; j < teams[i].size; j++)
        {
            if (teams[i].elements[j] == value)
            {
                return i;
            }
        }
    }
    return -1;
}

int add(tNode *newNode, tNode *currentNode)
{
    if (currentNode->next == NULL)
    {
        newNode->next = NULL;
        currentNode->next = newNode;
        return 0;
    }

    if (getTeam(currentNode->value) != getTeam(newNode->value))
    {
        add(newNode, currentNode->next);
    }
    else if (getTeam(currentNode->next->value) != getTeam(newNode->value))
    {
        newNode->next = currentNode->next;
        currentNode->next = newNode;
        return 0;
    }
    else
    {
        add(newNode, currentNode->next);
    }
    return 0;
}

int enqueue(int value)
{
    tNode *newNode;

    if (getTeam(value) == -1)
    {
        return -1;
    }

    if (!linkedListSize)
    {
        linkedList = (tNode *)malloc(sizeof(tNode));
        linkedList->value = value;
        linkedList->next = NULL;
        linkedListSize++;
        return linkedListSize;
    }

    newNode = (tNode *)malloc(sizeof(tNode));
    newNode->value = value;

    add(newNode, linkedList);
    linkedListSize++;

    return linkedListSize;
}

int dequeue()
{
    int aux;
    tNode *temp;

    if (linkedList == NULL)
    {
        return -1;
    }

    temp = linkedList;
    linkedList = temp->next;
    aux = (*temp).value;

    linkedListSize--;
    free(temp);

    return aux;
}

int getNewScenario(int scenario)
{
    int nelements = 0;
    char *cmd;
    int newElement;

    scanf("%i", &teamsSize);
    if (!teamsSize)
    {
        return 0;
    }

    teams = (tGroup *)malloc(sizeof(tGroup) * teamsSize);
    for (int i = 0; i < teamsSize; i++)
    {
        scanf("%i", &nelements);
        (teams + i)->size = nelements;
        (teams + i)->elements = (int *)malloc((sizeof(int) * nelements));
        for (int j = 0; j < nelements; j++)
        {
            scanf("%i", (teams + i)->elements + j);
        }
    }

    cmd = (char *)malloc(sizeof(char) * 7);

    printf("Scenario #%i\n", scenario + 1);
    while (1)
    {
        scanf("%s", cmd);
        if (!strcmp(cmd, "ENQUEUE"))
        {
            scanf("%i", &newElement);
            enqueue(newElement);
        }
        else if (!strcmp(cmd, "DEQUEUE"))
        {
            printf("%i\n", dequeue());
        }
        else if (!strcmp(cmd, "STOP"))
        {
            scenario++;
            printf("\n");

            linkedListSize = 0;
            teamsSize = 0;

            free(linkedList);
            free(teams);
            if (!getNewScenario(scenario))
            {
                return 0;
            }
        }
    }
}

int main(int argc, char **argv)
{
    return getNewScenario(0);
}