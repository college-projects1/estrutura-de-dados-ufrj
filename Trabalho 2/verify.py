def comp():
    f1 = open('2.out')
    a = f1.readlines()
    f1.close()

    f2 = open('2b.out')
    b = f2.readlines()
    f2.close()

    if(len(a)!= len(b)):
        print("C=", 0, "OK!")
        return

    c = 1
    for i in range(len(a)):
        if(a[i]!=b[i]):
            print(i, "A=", a[i], "B=", b[i])
        if(a[i]==b[i]):
            c+=1

    print("C=", c, "OK!")

comp()